using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    // a List of Powerups that can be edited in the inspector
    public List<Powerup> powerups;


    
    
    // public 'data' variable for TankData that lets us access variables inside TankData
    public TankData data;

    // 'Add' method that lets us add a new item to the Powerup list
    public void Add(Powerup powerup)
    {
        // Activates the powerup. If the powerup is permanent, it won't show up on the list but you'll permanently have the effect by doing it this way.
        powerup.OnActivate(data);

        // if the powerup is not permanent, run this code block
        if (!powerup.isPermanent)
        {
            // adds the powerup to the list
            powerups.Add(powerup);
        }
    }


    // 'Remove' method that lets us remove an item in the Powerup list
    public void Remove(Powerup powerup)
    {
        // deactivates the powerup
        powerup.OnDeactivate(data);
        // removes the powerup from the list
        powerups.Remove(powerup);
    }

    // Start is called before the first frame update
    void Start()
    {
        // initializes powerups
        powerups = new List<Powerup>();
    }

    // Update is called once per frame
    void Update()
    {
        // Create an List to hold our expired powerups
        List<Powerup> expiredPowerups = new List<Powerup>();

        // Loop through all the powers in the List
        foreach (Powerup power in powerups)
        {
            // Subtracts from the timer
            power.duration -= Time.deltaTime;

            // Assembles a list of expired powerups
            if (power.duration <= 0)
            {
                // Adds to the expired powerups list
                expiredPowerups.Add(power);
            }
        }
        // Now that we've looked at every powerup in our list, use our list of expired powerups to remove the expired ones.
        foreach (Powerup power in expiredPowerups)
        {
            // deactivates the expired powerups
            power.OnDeactivate(data);
            // removes the powerups from the list
            powerups.Remove(power);
        }
  
        // empties the expiredPowerups list
        expiredPowerups.Clear();

    }
}
