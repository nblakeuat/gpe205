using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    // Component for the Tank Motor. Gives the tank the ability to move and rotate.
    public TankMotor motor;
    // Component for the Tank Data. Holds our tank's data, such as the movespeed, turnspeed, etc.
    public TankData data;

    public TankFire fire;
    // creates 'InputScheme' enum, which will make the input scheme appear as a dropdown in the inspector, letting us choose between using WASD and arrowKeys
    public enum InputScheme { WASD, arrowKeys };
    // creates InputScheme 'input'variable which can use WASD or arrowKeys.
    public InputScheme input;





    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // switch that uses the 'input' variable
        switch (input)
        { 
            // case that uses WASD as the input scheme
            case InputScheme.WASD:


                // if the W key is pressed, run this code block
                if (Input.GetKey(KeyCode.W))
                {
                // sends a message to TankMotor that changes the moveSpeed to go forward
                    motor.Move(data.moveSpeed);
                }
                // If the S key is pressed, run this code block
                if (Input.GetKey(KeyCode.S))
                {
                // sends a message to TankMotor that changes the moveSpeed to go backwards
                motor.Move(-data.moveSpeed);
                }
                // if the D key is pressed, run this code block
                if (Input.GetKey(KeyCode.D))
                {
                // sends a message to TankMotor that changes the turnSpeed to go right
                motor.Rotate(data.turnSpeed);
                }
                // if the A key is pressed, run this code block
                if (Input.GetKey(KeyCode.A))
                {
                // sends a message to TankMotor that changes the turneSpeed to go left
                motor.Rotate(-data.turnSpeed);
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    fire.Shoot();
                }
                // break that stops the Tank's movement when any of the buttons are released
                break;

        }







        // switch that uses the 'input' variable
        switch (input)
        {
            // case that uses arrowKeys as the input scheme
            case InputScheme.arrowKeys:


                // if the Up arrow key is pressed, run this code block
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    // sends a message to TankMotor that changes the moveSpeed to go forward
                    motor.Move(data.moveSpeed);
                }
                // if the Down arrow key is pressed, run this code block
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    // sends a message to TankMotor that changes the moveSpeed to go backwards
                    motor.Move(-data.moveSpeed);
                }
                // if the Right arrow key is pressed, run this code block
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    // sends a message to TankMotor that changes the turnSpeed to go right
                    motor.Rotate(data.turnSpeed);
                }
                // if the Left arrow key is pressed, run this code block
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    // sends a message to TankMotor that changes the turneSpeed to go left
                    motor.Rotate(-data.turnSpeed);
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    fire.Shoot();
                }
                // break that stops the Tank's movement when any of the buttons are released
                break;

        }
    }
}
