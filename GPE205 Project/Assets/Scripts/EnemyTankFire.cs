using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTankFire : MonoBehaviour
{
    // creating 'projectile' Rigidbody that we will use for the shell.
    public Rigidbody projectile;
    // Timeout variable  for shell that is set at 3 by default.
    public float timeout = 3.0f;
    // private variable timeToNextfire that holds the time until you can fire again.
    private float timeToNextfire;
    // forceApplied variable  that holds the amount of force applied to the shell.
    public float forceApplied = 10f;
    // damage variable that holds the amount of damage the shell will do.
    public float damage = 50f;
    public TankData data;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Shoot()
    {
        // if Time.time is greater than timeToNextfire, run this code block
        if (timeToNextfire < Time.time)
        {
            // sets timeToNextfire as equal to the time + the fireRate, making it so the enemy will continously fire and that the firing rate is adjustable in the inspector
            timeToNextfire = Time.time + data.fireRate;
            // creates 'shell' Rigidbody which will be the shells that are fired
            Rigidbody shell;
            // instantiates the shell at the position and rotation of the attached transform
            shell = Instantiate(projectile, transform.position, transform.rotation);


            // this is what will shoot the shell forward from the position & rotation we instantiated the shell at. It shoots it forward and is multiplied by the forceApplied.
            shell.velocity = transform.TransformDirection(Vector3.forward * forceApplied);







            // after the timeout has passed, it will destroy the fired shell object
            Destroy(shell.gameObject, timeout);




        }



    }

    // Update is called once per frame
    void Update()
    {

        
    }
}
