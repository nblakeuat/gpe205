using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    // creating public static variable 'instance'
    public static GameManager instance;
    
   


    // A function that will check to see if GameManager already exists.
    void Awake()
    {
        // if GameManager doesn't already exist, run this code block
        if (instance == null)
        {
            // creates GameManager instance
            instance = this;
        }

        // If GameManager already exists, run this code block
        else
        {
            // Displays error message
            Debug.LogError("ERROR: There can only be one GameManager.");
            // Destroys the game object this GameManager is on
            Destroy(gameObject);
        }

    }




        

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
