using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController2 : MonoBehaviour
{
    // creating a Transform named 'target' that can be selected in the inspector
    public Transform target;
    // creating an array of transforms named 'waypoints' 
    public Transform[] waypoints;
    // creating int 'currentWaypoint' that tells us the current waypoint that the object is travelling to.
    private int currentWaypoint = 0;
    // creating a boolean named 'isPatrolForward' that keeps track of if the object is patrolling forward or not
    private bool isPatrolForward = true;
    // creating a float named 'closeEnough' that tells us the distance the object needs to be from a waypoint for it to be considered 'Close enough' to the waypoint to proceed to the next one.
    // can be adjusted in inspector
    public float closeEnough = 1.0f;
   
    // public 'motor' variable for TankMotor that lets us access variables inside TankMotor
    public TankMotor motor;
    // public 'data' variable for TankData that lets us access variables inside TankData
    public TankData data;
    // public 'enemyFire' variable for EnemyTankFire that lets us access variables inside EnemyTankFire
    public EnemyTankFire enemyFire;
    public enum AIState { Patrol, FireOnSight };
    public AIState aiState = AIState.Patrol;
    // creating 'LoopType' enum that holds 'Stop', 'Loop' and 'PingPong' which determiens what will occur when the object reaches the final waypoint
    public enum LoopType { Stop, Loop, PingPong };
    // makes it a public variable so you can select which one to use in the inspector
    public LoopType loopType;
    

    // creating an int named 'avoidanceStage' that keeps track of if the object is in avoidance mode or not
    private int avoidanceStage = 0;
    // creating a float named 'avoidanceTime' that holds the amount of time that the object will be in avoidance mode
    public float avoidanceTime = 2.0f;
    // creating int 'exitTime' that keeps track of how long until the player exits avoidance mode
    private float exitTime;
    // creating float named 'aiSenseRadius' that holds the value of the AI's sense radius. Adjustable in inspector.
    public float aiSenseRadius;
    // creating a float named 'stateEnterTime' that holds the amount of time the object will be in a state
    public float stateEnterTime;
    // private variable timeToNextfire that holds the time until you can fire again.
    private float timeToNextfire;



    // Start is called before the first frame update
    void Start()
    {

    }

    // DoAvoidance function that handles obstacle avoidance
    void DoAvoidance()
    {
        // if the avoidance stage is in stage 1, run this code block
        if (avoidanceStage == 1)
        {
            // causes the tank to rotate left
            motor.Rotate(-1 * data.turnSpeed);

            // If the tank is able to move forward, run this code block
            if (CanMove(data.moveSpeed))
            {
                // moves the avoidance stage onto stage 2
                avoidanceStage = 2;

                // Set the amount of time in second the avoidane stage will be in stage 2
                exitTime = avoidanceTime;
            }

            // Otherwise, we'll do this again next turn!
        }
        // if the avoidance stage is in stage 2, run this code block
        else if (avoidanceStage == 2)
        {
            // if the tank is able to move forward, run this code block
            if (CanMove(data.moveSpeed))
            {
                // Subtracts from our exit timer
                exitTime -= Time.deltaTime;
                // causes the tank to move forward
                motor.Move(data.moveSpeed);

                // If the tank has been moving long enough, run this code block
                if (exitTime <= 0)
                {
                    // sets the avoidance stage to 0, aka going out of avoidance mode
                    avoidanceStage = 0;
                }
            }
            // otherwise, run this code block
            else
            {
                // the tank can't move forward, so it will so back to stage 1
                avoidanceStage = 1;
            }
        }
    }

    // CanMove boolean that checks if we can move "speed" units forward. If so, returns true. If not, returns false.
    public bool CanMove(float speed)
    {
        // Cast a ray forward in the distance that we sent in
        RaycastHit hit;

        // If our raycast hits something, run this code block
        if (Physics.Raycast(transform.position, transform.forward, out hit, speed))
        {
            // if what we hit is not the player, run this code block
            if (!hit.collider.CompareTag("Player"))
            {
                // returns false, making it so the tank can't move
                return false;
            }
        }

        // otherwise, the tank will be able to move, so it will return true.
        return true;
    }

    // DoPatrol state
    void DoPatrol()
        {
            // If the object needs to rotate towards the waypoint, run this code block
            if (motor.RotateTowards(waypoints[currentWaypoint].position, data.turnSpeed))
            {
                // do nothing
                // this is so we know that we didn't accidentally leave this space empty and did it on purpose
            }
            else
            {
                // causes tha tank to move forward
                motor.Move(data.moveSpeed);
            }

            // If we are close enough to the waypoint, run this code block
            if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - transform.position) < (closeEnough * closeEnough))
            {

                // switch that uses the 'loopType' variable
                switch (loopType)
                {
                    // if the selected Loop Type is Stop, it will run everything inside this case
                    case LoopType.Stop:

                        // If currentWaypoint is less than the length of waypoints - 1, advance to the next waypoint, if we are still in range.
                        if (currentWaypoint < waypoints.Length - 1)
                        {
                            // adds to currentWaypoint by +1, meaning it will go to the next waypoinnt
                            currentWaypoint++;
                        }


                        break;
                }


                // switch that uses the 'loopType' variable
                switch (loopType)
                {
                    // if the selected Loop Type is Loop, it will run everything inside this case
                    case LoopType.Loop:


                        // If currentWaypoint is less than the length of waypoints - 1, advance to the next waypoint, if we are still in range.
                        if (currentWaypoint < waypoints.Length - 1)
                        {
                            // adds to currentWaypoint by +1, meaning it will go to the next waypoinnt
                            currentWaypoint++;
                        }

                        // otherwise, run this code block
                        else
                        {
                            // sets currentWayPoint as equal to 0
                            currentWaypoint = 0;
                        }

                        break;
                }

                switch (loopType)
                {
                    // if the selected Loop Type is PingPong, it will run everything inside this case
                    case LoopType.PingPong:

                        // if isPatrolForward is true, run this code block
                        if (isPatrolForward)
                        {
                            // If currentWaypoint is less than the length of waypoints - 1, advance to the next waypoint, if we are still in range.
                            if (currentWaypoint < waypoints.Length - 1)
                            {
                                // adds to currentWaypoint by +1, meaning it will go to the next waypoinnt
                                currentWaypoint++;
                            }
                            // otherwise, run this code block
                            else
                            {

                                // sets isPatrolForward as false
                                isPatrolForward = false;
                                // subtracts from currentWaypoint by -1, in other words, the object will travel to the previous waypoint
                                currentWaypoint--;
                            }
                        }

                        // if isPatrolForward is false, run this code block
                        else
                        {
                            // Advance to the next waypoint, if we are still in range
                            if (currentWaypoint > 0)
                            {
                                currentWaypoint--;
                            }
                            // otherwise, run this code block
                            else
                            {
                                // sets isPatrolForward as true
                                isPatrolForward = true;
                                // adds to currentWaypoint by +1, meaning it will go to the next waypoinnt
                                currentWaypoint++;
                            }
                        }
                        break;
                }

            }


    }


    // function that changes the current state the object is in
    public void ChangeState(AIState newState)
    {
        // sets the aiState as equal to new states, which changes the state
        aiState = newState;

        // saves the time the state was changed
        stateEnterTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {


        // Finite state machine for Patroller


        // if the AIState is in Patrol, run this code block
        if (aiState == AIState.Patrol)
        {
            // if the avoidance stage is not equal to 0, run this code block 
            if (avoidanceStage != 0)
            {
                // runs DoAvoidance Function
                DoAvoidance();
            }
            // otherwise, run this code block
            else
            {
                // runs DoPatrol state
                DoPatrol();

            }

                // if the target enters the AI's sense radius, run this code block
                 if (Vector3.Distance(target.position, transform.position) <= aiSenseRadius)
                 {
                // changes the state from Patrol to FireOnSight
                ChangeState(AIState.FireOnSight);

                 }
        }
        // if the AIState is FireOnSight, run this code block
        else if (aiState == AIState.FireOnSight)
        {
            // if the avoidance stage is not equal to 0, run this code block 
            if (avoidanceStage != 0)
            {
                // runs DoAvoidance Function
                DoAvoidance();
            }
            else
            {
                // runs 'Shoot' funtion in EnemyTankFire, causing the tank to shoot
                enemyFire.Shoot(); 
                // Sets timeToNextfire as the time when a shot was fired.
                timeToNextfire = Time.time;

            }

            // If the tank has been in the FireOnSight state for 10 seconds, run this code block
            if (Time.time >= stateEnterTime + 10)
            {
                // changes the AIState back to Patrol
                ChangeState(AIState.Patrol);
            }
        }
    }

}

