using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController3 : MonoBehaviour
{

    // creating a Transform named 'target' that can be selected in the inspector
    public Transform target;
    // creating a Transform 'tf'
    private Transform tf;


    // public 'enemyFire' variable for EnemyTankFire that lets us access variables inside EnemyTankFire
    public EnemyTankFire enemyFire;
    // public 'motor' variable for TankMotor that lets us access variables inside TankMotor
    public TankMotor motor;
    // public 'data' variable for TankData that lets us access variables inside TankData
    public TankData data;

    // creating a float named 'fleeDistance' that can be adjusted in the inspector. Keeps track of the distance the object will flee
    public float fleeDistance = 1.0f;
    // creating an int named 'avoidanceStage' that keeps track of if the object is in avoidance mode or not
    private int avoidanceStage = 0;
    // creating a float named 'avoidanceTime' that holds the amount of time that the object will be in avoidance mode
    public float avoidanceTime = 2.0f;
    // creating int 'exitTime' that keeps track of how long until the player exits avoidance mode
    private float exitTime;

    // creating "AIState' enum that determines what state the object will use
    public enum AIState { Chase, ChaseAndFire, CheckForFlee, Flee, Rest };
    // makes it a public variable so you can select which one to use in the inspector, is 'Chase' by default
    public AIState aiState = AIState.Chase;
    // creating a float named 'stateEnterTime' that holds the amount of time the object will be in a state
    public float stateEnterTime;
    // creating float named 'aiSenseRadius' that holds the value of the AI's sense radius.  Adjustable in inspector
    public float aiSenseRadius;
    // creating float named 'restingHealRate' that holds the value of hp that heals per second while the object is in the 'Rest' state
    public float restingHealRate;
    // private variable timeToNextfire that holds the time until you can fire again.
    private float timeToNextfire;

    // creating 'Awake' function
    void Awake()
    {
        // saves our transform in a variable in this function to save the processing
        tf = gameObject.GetComponent<Transform>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }


    // CheckForFlee state
    public void CheckForFlee()
    {
        // This is here in case we need to add something in the future
    }

    // DoRest state
    public void DoRest()
    {
        // Regenerates our health per second
        data.health += restingHealRate * Time.deltaTime;

        // prevents the regenerated health from going over the max health
        data.health = Mathf.Min(data.health, data.maxHealth);
    }

    // function that changes the current state the object is in
    public void ChangeState(AIState newState)
    {
        // sets the aiState as equal to new states, which changes the state
        aiState = newState;

        // saves the time the state was changed
        stateEnterTime = Time.time;
    }


    // DoChase state, makes the tank chase the target
    void DoChase()
    {
        // makes the tank rotate toward the target
        motor.RotateTowards(target.position, data.turnSpeed);


        // if we can move forward by "data.moveSpeed" units, run this code block
        if (CanMove(data.moveSpeed))
        {
            // causes the tank to move forward
            motor.Move(data.moveSpeed);
        }
        // otherwise, run this code block
        else
        {
            // Enters obstacle avoidance stage 1
            avoidanceStage = 1;
        }
    }

    // DoFlee state
    void DoFlee()
    {
        // The vector3 "vectorToTarget"  = target position - our position
        Vector3 vectorToTarget = target.position - transform.position;

        // flips this vector by -1 to get a vector away from our target, aka moving away from our target
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

        // normalizes the vector to give it a magnitude of 1
        vectorAwayFromTarget.Normalize();

        // Multiplies the normalized vector by fleeDistance to make a vector the same length as fleeDistance
        vectorAwayFromTarget *= fleeDistance;

        // We can find the position in space we want to move to by adding our vector away from our AI to our AI's position.
        //     This gives us a point that is "that vector away" from our current position.
        Vector3 fleePosition = vectorAwayFromTarget + transform.position;
        // makes the tank  rotate away from the fleePosition
        motor.RotateTowards(fleePosition, data.turnSpeed);
        // makes the tank move away from the fleePosition, aka fleeing from the target
        motor.Move(data.moveSpeed);



    }

    // DoAvoidance function that handles obstacle avoidance
    void DoAvoidance()
    {
        // if the avoidance stage is in stage 1, run this code block
        if (avoidanceStage == 1)
        {
            // causes the tank to rotate left
            motor.Rotate(-1 * data.turnSpeed);

            // If the tank is able to move forward, run this code block
            if (CanMove(data.moveSpeed))
            {
                // moves the avoidance stage onto stage 2
                avoidanceStage = 2;

                // Set the amount of time in second the avoidane stage will be in stage 2
                exitTime = avoidanceTime;
            }

            // Otherwise, we'll do this again next turn!
        }
        // if the avoidance stage is in stage 2, run this code block
        else if (avoidanceStage == 2)
        {
            // if the tank is able to move forward, run this code block
            if (CanMove(data.moveSpeed))
            {
                // Subtracts from our exit timer
                exitTime -= Time.deltaTime;
                // causes the tank to move forward
                motor.Move(data.moveSpeed);

                // If the tank has been moving long enough, run this code block
                if (exitTime <= 0)
                {
                    // sets the avoidance stage to 0, aka going out of avoidance mode
                    avoidanceStage = 0;
                }
            }
            // otherwise, run this code block
            else
            {
                // the tank can't move forward, so it will so back to stage 1
                avoidanceStage = 1;
            }
        }
    }

    // CanMove - checks if I can move "speed" units forward. If so, returns true. If not, returns false.
    public bool CanMove(float speed)
    {
        // Cast a ray forward in the distance that we sent in
        RaycastHit hit;

        // If our raycast hits something, run this code block
        if (Physics.Raycast(transform.position, transform.forward, out hit, speed))
        {
            // if what we hit is not the player, run this code block
            if (!hit.collider.CompareTag("Player"))
            {
                // returns false, making it so the tank can't move
                return false;
            }
        }

        // otherwise, the tank will be able to move, so it will return true.
        return true;
    }










    // Update is called once per frame
    void Update()
    {




        // Finite State Machine for Shotgun type


        // if the AIState is Chase, run this code block
        if (aiState == AIState.Chase)
        {
            // if the avoidanceStage is not equal to 0, run this code block
            if (avoidanceStage != 0)
            {
                // runs DoAvoidance Function
                DoAvoidance();
            }
            // otherwise, run this code block
            else
            {
                // runs DoChase state
                DoChase();
            }

            // if the player's health is less than the maxHealth * 0.3, run this code block
            if (data.health < data.maxHealth * 0.3f)
            {
                // changes the state to CheckforFlee
                ChangeState(AIState.CheckForFlee);
            }
            // if the target is in the AI's sense radius, run this code block
            else if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                // changes the state to ChaseAndFire
                ChangeState(AIState.ChaseAndFire);
            }
        }
        // if the AI's state is ChaseAndFire, run this code  block
        else if (aiState == AIState.ChaseAndFire)
        {
            // if the avoidance stage is not equal to 0, run this code block
            if (avoidanceStage != 0)
            {
                // runs DoAvoidance function
                DoAvoidance();
            }
            // otherwise, run this code block
            else
            {
                // runs DoChase state
                DoChase();

                // sets the fire rate as 0, which is what creates the Shotgun effect
                data.fireRate = 0;
                // runs 'Shoot' function inside of EnemyTankFire, making it so the tank will fire a bullet
                enemyFire.Shoot();
              
                
            }
            // if the player's health is less than the maxHealth * 0.3, run this code bloc
            if (data.health < data.maxHealth * 0.3f)
            {
                // changes the state to CheckForFlee
                ChangeState(AIState.CheckForFlee);
            }
            // if the target enters the AI's sense radius, run this code block
            else if (Vector3.Distance(target.position, tf.position) > aiSenseRadius)
            {
                // changes the state to 'Chase' state
                ChangeState(AIState.Chase);
            }
        }
        // if the AI state is Flee, run this code block
        else if (aiState == AIState.Flee)
        {
            // if the avoidance stage is not equal to 0, run this code block
            if (avoidanceStage != 0)
            {
                // runs DoAvoidance function
                DoAvoidance();
            }
            // otherwise, run this code block
            else
            {
                // runs DoFlee state
                DoFlee();
            }

            // if the player has been in the Flee state for 30 seconds, run this code block
            if (Time.time >= stateEnterTime + 30)
            {
                // changes state to CheckForFlee
                ChangeState(AIState.CheckForFlee);
            }
        }
        // if the AI State is CheckForFlee, run this code block
        else if (aiState == AIState.CheckForFlee)
        {
            // runs CheckForFlee state
            CheckForFlee();

            // if the target is in the AI's sense radius while in CheckForFlee mode, run this code block
            if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                // Changes the state to Flee
                ChangeState(AIState.Flee);
            }
            // If the target is not in the AI's sense radius while in CheckforFlee mode, run this ode block
            else
            {
                // changes the state to Rest 
                ChangeState(AIState.Rest);
            }
        }
        // if the AI State is Rest, run this code block
        else if (aiState == AIState.Rest)
        {
            // runs DoRest state
            DoRest();

            // if the target is in the AI's sense radius while in Rest state, run this code block
            if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                // Changes the state to Flee
                ChangeState(AIState.Flee);
            }
            // if the AI reaches max health, run this code block
            else if (data.health >= data.maxHealth)
            {
                // Changes the state to Chase
                ChangeState(AIState.Chase);
            }
        }
    }
}
