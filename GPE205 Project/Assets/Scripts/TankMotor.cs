using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour
{


    // Holds our Character Controller component
    private CharacterController characterController;


    // Start is called before the first frame update
    void Start()
    {
        // Stores CharacterController in the  variable
        characterController = gameObject.GetComponent<CharacterController>();
    }

    // the 'Move' function will make the tank move forward
    public void Move(float speed)
    {
        // Initializing a Vector3 "speedVector" that sets the transform direction as forward, meaning the tank will move forward or backwards.
        Vector3 speedVector = transform.TransformDirection(Vector3.forward);

        // Calls SimpleMove and sends it speedVector, which is multplied by speed to change the speed of the tank
        characterController.SimpleMove(speedVector * speed);
    }


    // the 'Rotate' function will make the tank rotate
    public void Rotate(float speed)
    {
        // Vector3 that holds the rotation data and sets it as equal to Vector3.up so it rotates around the Y axis.
        Vector3 rotateVector = Vector3.up;





        // changes the rotation to "per second" instead of "per frame", holds the number of seconds it took to draw the last frame.
        rotateVector *= Time.deltaTime;


        // Multiplies rotateVector by speed to change the rotation speed of the tank and rotates the tank by the 'Space.self' value
        transform.Rotate(rotateVector * speed, Space.Self);
    }





    
    // RotateTowards (Target, speed) - rotates towards the target (if possible).
    // If we rotate, then returns true. If we can't rotate (because we are already facing the target) return false.
    public bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 vectorToTarget;

        // The vector to our target is the DIFFERENCE between the target position and our position.
        //   How would our position need to be different to reach the target? "Difference" is subtraction!
        vectorToTarget = target - transform.position;

        // Find the Quaternion that looks down that vector
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);

        // If that is the direction we are already looking, we don't need to turn!
        if (targetRotation == transform.rotation)
        {
            return false;
        }

        


            // Otherwise:
            // Change our rotation so that we are closer to our target rotation, but never turn faster than our Turn Speed
            //   Note that we use Time.deltaTime because we want to turn in "Degrees per Second" not "Degrees per Framedraw"
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, speed * Time.deltaTime);
        
        // We rotated, so return true
        return true;
    }




    // Update is called once per frame
    void Update()
    {
    

    }


}
