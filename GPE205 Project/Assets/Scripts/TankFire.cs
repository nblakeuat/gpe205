using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankFire : MonoBehaviour
{
    


    // Start is called before the first frame update
    void Start()
    {

    }
   
    // creating 'projectile' Rigidbody that we will use for the shell.
    public Rigidbody projectile;
    // Timeout variable  for shell that is set at 3 by default.
    public float timeout = 3.0f;

    public TankData data;
  
    // private variable timeToNextfire that holds the time until you can fire again.
    private float timeToNextfire;
    // forceApplied variable  that holds the amount of force applied to the shell.
    public float forceApplied = 10f;
    // damage variable that holds the amount of damage the shell will do.
    public float damage = 25.0f;


    public void Shoot()
    {
        // If Spacebar is pressed , it will run everything inside this code block
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // if timeToNextFire is equal to 0, run this code block
            if (timeToNextfire <= 0)
            {
                timeToNextfire = data.fireRate;
                // creates 'shell' Rigidbody which will be the shells that are fired
                Rigidbody shell;
                // instantiates the shell at the position and rotation of the attached transform
                shell = Instantiate(projectile, transform.position, transform.rotation);


                // this is what will shoot the shell forward from the position & rotation we instantiated the shell at. It shoots it forward and is multiplied by the forceApplied.
                shell.velocity = transform.TransformDirection(Vector3.forward * forceApplied);







                // after the timeout has passed, it will destroy the fired shell object
                Destroy(shell.gameObject, timeout);




            }
            // else if the timeToNextFire is greater than 0, run this code block
            else if (timeToNextfire > 0)
            {
                // displays debug log message telling you the shell is on cooldown.
                Debug.Log("Please wait! Your fire is on cooldown!");

            }
        }
    }



    // Update is called once per frame
    void Update()
    {
        // sets timeToNextfire as -= to Time.deltaTime, which is the number of seconds it takes to draw the last frame.
        timeToNextfire -= Time.deltaTime;


       
  
    }
}

