using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShellCollision : MonoBehaviour
{
    // public 'data' variable for TankData that lets us access variables inside TankData
    public TankData data;
    // public 'enemyFire' variable for EnemyTankFire that lets us access variables inside EnemyTankFire
    public EnemyTankFire enemyFire;


    // OnCollision function
    void OnCollisionEnter(Collision collision)
    {
        // if the player's shell collides with a player's tank, run this code block
        if (collision.gameObject.CompareTag("Player"))
        {
            // decreases the player's health by the enemy shell's damage set in the inspector
            data.health -= enemyFire.damage;
            // destroys the enemy's shell after colliding with the player
            Destroy(gameObject);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
