using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Serializes the class so it shows up in the inspector
[System.Serializable]
public class Powerup
{
    // public float that will modify the speed of the tank
    public float speedModifier;
    // public float that will modify the health of the tank
    public float healthModifier;
    // public float that will modify the max health of the tank
    public float maxHealthModifier;
    // public float that will modify the fire rate of the tank
    public float fireRateModifier;

   
    // public float that determines how long the powerup will last
    public float duration;
    // public boolean that determines if the powerup will be permanent or not
    public bool isPermanent;

    // creating OnActivate function that uses TankData and Tankfire
    public void OnActivate(TankData data)
    {
        // moveSpeed = moveSpeed + speedModifier
        data.moveSpeed += speedModifier;
        // health = health + healthModifier
        data.health += healthModifier;
        if (data.health >= data.maxHealth)
        {
            // sets health as equal to max health, making it so that if a powerup makes you gain health, it makes it you'll never go over the max amount of health.
            data.health = data.maxHealth;
        }
        // maxHealth = maxHealth + maxHealthModifier
        data.maxHealth += maxHealthModifier;
        // fireRate = fireRate + fireRateModifier
        data.fireRate += fireRateModifier;

       
    }

    // creating OnDeactivate function that uses TankData and Tankfire
    public void OnDeactivate(TankData data)
    {
        // moveSpeed = moveSpeed - speedModifier
        data.moveSpeed -= speedModifier;
        // health = health - healthModifier
        data.health -= healthModifier;
        // maxHealth = maxHealth - maxHealthModifier
        data.maxHealth -= maxHealthModifier;
        // fireRate = fireRate - fireRateModifier 
        data.fireRate -= fireRateModifier;

        
    }

}
