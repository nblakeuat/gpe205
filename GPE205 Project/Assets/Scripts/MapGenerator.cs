using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator : MonoBehaviour
{
    // int for the amount of rows on the map that can be edited via inspector
    public int rows;
    // int for the amount fo colums on the map that can be edited via inspector
    public int cols;
    // private roomWidth float that holds the width of every room. Set at 50.
    private float roomWidth = 50.0f;
    // private roomHeight float that holds the height of every room. Set at 50.
    private float roomHeight = 50.0f;
    // two dimensional array that uses two numbers to refer to the location
    private Room[,] grid;
    // int for the mapSeed which determines the seed of the map. Can be edited via inspector
    public int mapSeed;

    // grid prefabs that will be used to build the map. Selectable via inspector.
    public GameObject[] gridPrefabs;
    // isMapOfTheDay boolean that determines if the map is the Map of The Day or not
   public bool isMapOfTheDay;

    // isRandomLevel boolean that determines if the map is a random level or not.
    public bool isRandomLevel;
    // DateToInt function that adds up our year, month, and day and returns it as an integer.
    public int DateToInt(DateTime dateToUse)
    {
        // Adds our date up and returns it as an integer.
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
        
    }

    // GenerateGrid function that will generate our map
    public void GenerateGrid()
    {
        // Sets the map seed
        UnityEngine.Random.InitState(mapSeed);

        // Clears out the grid using the cols and rows selected in the inspecto
        grid = new Room[cols, rows];

       

        // For each grid row, run this loop
        for (int i = 0; i < rows; i++)
        {
            // for each column in that row, run this loop
            for (int j = 0; j < cols; j++)
            {
                // Figures out the x position by multiplying the room width and j
                float xPosition = roomWidth * j;
                // figures out the z position by multiplying the room height and i
                float zPosition = roomHeight * i;
                // Determines the location using the x position and z position
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                // Create a new grid at this location
                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;

                // Sets its parent
                tempRoomObj.transform.parent = this.transform;

                // Gives it a meaningful name
                tempRoomObj.name = "Room_" + j + "," + i;

                // Get the room object
                Room tempRoom = tempRoomObj.GetComponent<Room>();

                // The code below will open the doors
                // If we are on the bottom row, run this code block 
                if (i == 0)
                {
                    // opens the north door
                    tempRoom.doorNorth.SetActive(false);
                }
                // If we are on the top row, run this code block
                else if (i == rows - 1)
                {
                    // opens the south door
                    tempRoom.doorSouth.SetActive(false);
                }
                // otherwise we'll be in the middle, so run this code block
                else
                {
                   // opens the north door
                    tempRoom.doorNorth.SetActive(false);
                    // opens the south door
                    tempRoom.doorSouth.SetActive(false);
                }
                // If we are on the first column, open the east door
                if (j == 0)
                {
                    // opens the east door
                    tempRoom.doorEast.SetActive(false);
                }
                // if we're on the first column, run this code block
                else if (j == cols - 1)
                {
                    // opens the west door
                    tempRoom.doorWest.SetActive(false);
                }
                // otherwise we'll be in the middle, so run this code block
                else
                {
                    
                    // opens east door
                    tempRoom.doorEast.SetActive(false);
                    // opens west door
                    tempRoom.doorWest.SetActive(false);
                }
                // Saves this data to the grid array
                grid[j, i] = tempRoom;
            }
        }

    }
        // RandomRoomPrefab function that returns the room
        public GameObject RandomRoomPrefab()
        {
            // returns a random room using the grid prefabs
            return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
        }








        // Start is called before the first frame update
        void Start()
        {

        // Initstates the date
        UnityEngine.Random.InitState(DateToInt(DateTime.Now));

        // if the map is map of the day, run this code block
        if (isMapOfTheDay)
         {
            // sets the map seed to the current date, making it so the map changes every day
            mapSeed = DateToInt(DateTime.Now.Date);
         }

        // if the map is a random level, run this code block
        else if (isRandomLevel == true)

        {
            // sets the mapSeed as a random number inbetween 1 and 9999, making it almost guranteed that yours always gonna get a different map
            mapSeed = UnityEngine.Random.Range(1, 9999);
            
        }
         // Generate Grid
         GenerateGrid();
        }

        // Update is called once per frame
        void Update()
        {

        }
    
}
