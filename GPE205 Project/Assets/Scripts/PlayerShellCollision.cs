using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShellCollision : MonoBehaviour
{


    // public 'data' variable for TankData that lets us access variables inside TankData
    public TankData data;
    // public 'data' variable for TankData that lets us access variables inside TankData
    public TankData data2;
    // public 'data' variable for TankData that lets us access variables inside TankData
    public TankData data3;
    // public 'data' variable for TankData that lets us access variables inside TankData
    public TankData data4;
    // public 'data' variable for TankData that lets us access variables inside TankData
    public TankFire fire;

    // OnCollision function
    void OnCollisionEnter(Collision collision)
    {
        // if the enemy's shell collides with an enemy's tank, run this code block
        if (collision.gameObject.CompareTag("Enemy"))
        {
            // decreases the enemy's health by the player's shell's damage set in the inspector
            data.health -= fire.damage;
            // destroys the player's shell after colliding with the enemy
            Destroy(gameObject);
        }
        else if (collision.gameObject.CompareTag("PatrolEnemy"))
        {
                // decreases the enemy's health by the player's shell's damage set in the inspector
                data2.health -= fire.damage;
                // destroys the player's shell after colliding with the enemy
                Destroy(gameObject);
        }
        else if (collision.gameObject.CompareTag("CowardEnemy"))
        {
            // decreases the enemy's health by the player's shell's damage set in the inspector
            data3.health -= fire.damage;
            // destroys the player's shell after colliding with the enemy
            Destroy(gameObject);
        }

        else if(collision.gameObject.CompareTag("ShotgunEnemy"))
        {
            // decreases the enemy's health by the player's shell's damage set in the inspector
            data4.health -= fire.damage;
            // destroys the player's shell after colliding with the enemy
            Destroy(gameObject);
        }
    }






    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
