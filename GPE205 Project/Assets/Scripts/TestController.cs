using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestController : MonoBehaviour
{
   

    // Component for the Tank Motor. Gives the tank the ability to move and rotate.
    public TankMotor motor;
    // Component for the Tank Data. Holds our tank's data, such as the movespeed, turnspeed, etc.
    public TankData data;  

    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        // Links up the Move function in TankMotor to the movespeed in TankData
        motor.Move(data.moveSpeed);
        // Links up the Rotate function in TankMotor to the turnspeed in Tankdata
        motor.Rotate(data.turnSpeed);
    }


    

}
