using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    
    



    // sets 'moveSpeed', the default moving speed to 3 meters per second.
    public float moveSpeed = 3;
    // sets 'turnSpeed', the default rotating speed to 180 meters per second.
    public float turnSpeed = 180;
    
    // creates public float for maxHealth which is set at 100 by default. is adjustable in inspector
    public float maxHealth = 100;
    
    public float health; // creates a public float for health which is hidden in the inspector
    // creates a public float for the score gained from destroying tanks. is adjustable in inspector
    public float scoreGain = 50;
    // creates a public float for the score which is adjustable in the inspector
    public float score;
    // fireRate variable for the shell that is set at 3 by default. This is the number of seconds inbetween shell shots.
    public float fireRate = 3.0f;




    // Start is called before the first frame update
    void Start()
    {
        // sets the current health as equal to the maximum health
        health = maxHealth;
        // sets the score as 0
        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // when health is less than or equal to 0, run this code block
        if (health <= 0)
        {
            // destroys the game object, which is the tank in this case
            Destroy(gameObject);
            // adds to the score by the scoreGain that was set in the inspector
            score += scoreGain;
        }
    }
}
