using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Respawn : MonoBehaviour
{
    // GameObject for the player. Can be selected via inspector.
    public GameObject playerObject;
    // array of GameObjects for the spawn Points
    private GameObject[] spawnPoints;

    // private GameObject 'spawnedPlayer' that holds that instantiated player character
    private GameObject spawnedPlayer;

    // the player's time until they respawn if they die. Can be adjusted via inspector
    public float playerRespawnDelay;

    // private float for the time until the player's next respawn
    private float playerRespawnTime;
    

    // SpawnPlayer function
   public void SpawnPlayer()
    {
          // Finds all objects with the tag 'Spawn Point'
            spawnPoints = GameObject.FindGameObjectsWithTag("Spawn Point");


        // Makes it so that the random spawn is never the same value and changes every milisecond.
        Random.InitState(System.DateTime.Now.Millisecond);
        // spawn interger that's a random number from the added spawnpoints on the map
        int spawn = Random.Range(0, spawnPoints.Length);
    
        // Instantiates the player at the spawnpoint that corresponds with the spawn interger
        spawnedPlayer = Instantiate(playerObject, spawnPoints[spawn].transform.position, Quaternion.identity) as GameObject;
    }

    // GameObject for the scout enemy. Can be selected via inspector.
    public GameObject scoutObject;
    // array of GameObjects for the scout spawn points
    private GameObject[] scoutSpawns;
    // private GameObject 'spawnedScout' that holds that instantiated scout enemy
    private GameObject spawnedScout;

    // SpawnScout function
    public void SpawnScout()
    {
        // Finds all objects with the tag 'Enemy Spawn'
        scoutSpawns = GameObject.FindGameObjectsWithTag("Enemy Spawn");


        // Makes it so that the random spawn is never the same value and changes every milisecond.
        Random.InitState(System.DateTime.Now.Millisecond);
        // spawnscout interger that's a random number from the added scoutSpawns on the map
        int spawnscout = Random.Range(0, scoutSpawns.Length);

        // Instantiates the scout at the scout spawn that corresponds with the spawnscout interger
        spawnedScout = Instantiate(scoutObject, scoutSpawns[spawnscout].transform.position, Quaternion.identity) as GameObject;
    }

    // GameObject for the patrol enemy. Can be selected via inspector.
    public GameObject patrolObject;
    // array of GameObjects for the patrol spawn points
    private GameObject[] patrolSpawns;
    // private GameObject 'spawnedPatrol' that holds the instantiated patrol enemy
    private GameObject spawnedPatrol;

    // SpawnPatrol function
    public void SpawnPatrol()
    {
        // Finds all objects with the tag 'Patrol Spawn'
        patrolSpawns = GameObject.FindGameObjectsWithTag("Patrol Spawn");


        // Makes it so that the random spawn is never the same value and changes every milisecond.
        Random.InitState(System.DateTime.Now.Millisecond);
        // spawnpatrol interger that's a random number from the added patrolSpawns on the map
        int spawnpatrol = Random.Range(0, patrolSpawns.Length);

        // Instantiates the patrol enemy at the patrol spawn that corresponds with the spawnpatrol interger
        spawnedPatrol = Instantiate(patrolObject, patrolSpawns[spawnpatrol].transform.position, Quaternion.identity) as GameObject;
    }

    // GameObject for the shotgun enemy. Can be selected via inspector.
    public GameObject shotgunObject;
    // array of GameObjects for the shotgun spawn points
    private GameObject[] shotgunSpawns;
    // private GameObject 'spawnedPatrol' that holds the instantiated shotgun enemy
    private GameObject spawnedShotgun;

    // SpawnShotgun function
    public void SpawnShotgun()
    {
        // Finds all objects with the tag 'Shotgun Spawn'
        shotgunSpawns = GameObject.FindGameObjectsWithTag("Shotgun Spawn");


        // Makes it so that the random spawn is never the same value and changes every milisecond.
        Random.InitState(System.DateTime.Now.Millisecond);
        // spawnshotgun interger that's a random number from the added shotgunSpawns on the map
        int spawnshotgun = Random.Range(0, shotgunSpawns.Length);

        // Instantiates the shotgun enemy at the shotgun spawn that corresponds with the spawnshotgun interger
        spawnedShotgun = Instantiate(shotgunObject, shotgunSpawns[spawnshotgun].transform.position, Quaternion.identity) as GameObject;
    }

    // GameObject for the coward enemy. Can be selected via inspector.
    public GameObject cowardObject;
    // array of GameObjects for the coward spawn points
    private GameObject[] cowardSpawns;
    // private GameObject 'spawnedCoward' that holds the instantiated coward enemy
    private GameObject spawnedCoward;

    // SpawnCoward function
    public void SpawnCoward()
    {
        // Finds all objects with the tag 'Coward Spawn'
        cowardSpawns = GameObject.FindGameObjectsWithTag("Coward Spawn");


        // Makes it so that the random spawn is never the same value and changes every milisecond.
        Random.InitState(System.DateTime.Now.Millisecond);
        // spawncoward interger that's a random number from the added cowardSpawns on the map
        int spawncoward = Random.Range(0, cowardSpawns.Length);

        // Instantiates the coward enemy at the coward spawn that corresponds with the spawncoward interger
        spawnedCoward = Instantiate(cowardObject, cowardSpawns[spawncoward].transform.position, Quaternion.identity) as GameObject;

    }

    // wait function
    IEnumerator wait()
    {
        // waits for 1 second.
        yield return new WaitForSeconds(1);

            SpawnPlayer();
        
    }

    // enemyWait function
    IEnumerator enemyWait()
    {
        // waits for 3 seconds to execute code below
        yield return new WaitForSeconds(3);
        // runs SpawnScout function
            SpawnScout();
        // waits for 3 seconds to execute code below
        yield return new WaitForSeconds(3);
        // runs SpawnPatrol function
        SpawnPatrol();
        // waits for 3 seconds to execute code below
        yield return new WaitForSeconds(3);
        // runs SpawnShotgun function
        SpawnShotgun();
        // waits for 3 seconds to execute code below
        yield return new WaitForSeconds(3);
        // runs SpawnCoward function
        SpawnCoward();
    }





        // Start is called before the first frame update
        void Start()
        {
        // runs wait function
        StartCoroutine(wait());
        // runs enemyWait function
        StartCoroutine(enemyWait());

        playerRespawnTime = Time.time + playerRespawnDelay;

        }



    // Update is called once per frame
    void Update()
    {

        // if the spawnedPlayer is destroyed/doesn't exist, run this code block
        if (spawnedPlayer == null)
        {
            // and if  the respawn timer is up, run this code block
            if (Time.time > playerRespawnTime)
            {
                // runs SpawnPlayer function
                SpawnPlayer();

                // sets the time until next respawn back to full
                playerRespawnTime = Time.time + playerRespawnDelay;
            }   
        }

        // otherwise, if the tank does exist/is alive, it will postpone the respawn.
        else
        {
            // sets the time until next respawn back to full
            playerRespawnTime = Time.time + playerRespawnDelay;

        }
    }
}
