using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour
{
    // array of pickup prefabs that can be selected in the inspector
    public GameObject[] pickupPrefab;
    // interger that determines what the numerical value of current pickup is
    private int currentPickup;
    // float for the spawn delay time that can be set in the inspector
    public float spawnDelay;
    // private float for the time that the item will spawn after the first one
    private float nextSpawnTime;
    // transform variable
    private Transform tf;
    // private spawnedPickup variable for instantiating the pickup
    private GameObject spawnedPickup;


    // Start is called before the first frame update
    void Start()
    {
        // sets the transform variable
        tf = gameObject.GetComponent<Transform>();
        // sets the next spawn time
        nextSpawnTime = Time.time + spawnDelay;
    }

    // Update is called once per frame
    void Update()
    {
        // If no pickup is spawned, run this code block
        if (spawnedPickup == null)
        {
            // And if it's also time to spawn the pickup, run this code block
            if (Time.time > nextSpawnTime)
            {
                // Instantiates the pickup 
                spawnedPickup = Instantiate(pickupPrefab[currentPickup], tf.position, Quaternion.identity) as GameObject;

                // if the current pickup has been picked up, run this code block
                if (currentPickup < pickupPrefab.Length - 1)
                {
                   // moves onto the next Pickup
                    currentPickup++;
                }
                
                // otherwise, run this code block
                   else 
                {
                    // sets currentPickup back to 0, making it so that it'll loop after the last powerup.
                    currentPickup = 0;
                }

               // sets the next spawn time
                nextSpawnTime = Time.time + spawnDelay;
            }
        }
        else
        {
            // Otherwise, the object still exists, so postpone the spawn
            nextSpawnTime = Time.time + spawnDelay;
        }
    }
}
