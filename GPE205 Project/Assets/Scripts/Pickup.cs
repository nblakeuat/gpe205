using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    // public 'powerup' variable which lets us access anything in the Powerup class
    public Powerup powerup;
    // 'feedback' Audio clip that we'll use to play a sound when the pickup is collected.
    public AudioClip feedback;


    // OnTriggerEnter function
    public void OnTriggerEnter(Collider other)
    {
        // Store another object's PowerupController component if it has one
        PowerupController powCon = other.GetComponent<PowerupController>();

        // If the other object has a PowerupController, run this code block
        if (powCon != null)
        {
            // Adds the powerup to the PowerupController list
            powCon.Add(powerup);

            // Play Feedback (if it is set)
            if (feedback != null)
            {
                // plays the audio clip attacked to feedback at the position of the pickup when it's collected
                AudioSource.PlayClipAtPoint(feedback, transform.position, 1.0f);
            }


            // Destroy the pickup after it's picked up
            Destroy(gameObject);
        }
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
